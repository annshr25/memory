//Переворот карточки по клику
function flipCards({frontClass}) {

	function changeClass(element) {
	  	if(element.classList.contains("card")){
	  		element.classList.toggle(frontClass);
	  	}	  	
	  	var target = element.parentElement;
	  	if(target.classList.contains("card")){
	  		target.classList.toggle(frontClass);
	  	}    	
	}	 

	let cardsArray = Array.from(document.querySelectorAll('div.card'));

 	cardsArray.forEach(function(cardOne) {
		cardOne.addEventListener("click", function(event) {
		    if (event.target.tagName === "DIV") {
		      changeClass(event.target);
		    }
		});
	});
}
// Логика игры
function setTimer(){
	let cardsArray = Array.from(document.querySelectorAll('div.card'));
	var timer = document.querySelector(".timer");
	var minuteValue = 1;
	var secondValue = 60;
	timer.innerHTML = "0"+minuteValue+":"+ "00";
	var isStartGame = false;
	var isWinGame=false;
	let timerId;
	var timerGame;

	var modalWindow = document.getElementById("modal-window");
	var textWindow = document.getElementById("window-text");
	var buttonWindow = document.getElementById("window-button");
	// 	Анимация текста
	function animateText(textWindow){
		var letters = textWindow.innerHTML;
		letters = letters.split("");
		console.log(letters);
		textWindow.innerHTML = "";
		var offset = 0;	
		var i = 0;				
		letters.forEach(function(letter){
			i=i+500;
			setTimeout(() => {
							var wrap = document.createElement("span");
						  	wrap.innerText = letter;
							textWindow.appendChild(wrap);
							wrap.classList.add("waveanim");
						}, i);			
		});
	}				
	// Начало новой игры
	buttonWindow.addEventListener("click",function(event){
		modalWindow.style.display = "none";
		cardsArray.forEach(function(cardOne) {
			cardOne.classList.remove("non_clicked");
			cardOne.classList.remove("front-rotate");
			var back = cardOne.querySelector('div.back');
			back.classList.remove("wrong_click");
			back.classList.remove("right_click");
		});
		fillCards();
		secondValue = 60;
		timer.innerHTML = "0"+minuteValue+":"+ "00";
	});

	// Работа таймера и всплывающие окна
	cardsArray.forEach(function(cardOne) {
		cardOne.addEventListener("click", function(event) {			
			if(!isStartGame){
				timerId = setInterval (()=>{
					secondValue--;
					if(secondValue<10){
						timer.innerHTML = "00:"+ "0"+secondValue;
					}
					else{
						timer.innerHTML = "00:"+ secondValue;
					}
				},1000);				
				isStartGame = true;
				isWinGame = false;
				timerGame = setTimeout(() => {
					timer.innerHTML = "00:00";
					clearInterval(timerId);
					textWindow.innerHTML="Lose";
					buttonWindow.innerHTML = "Try again";
					modalWindow.style.display = "flex";
					animateText(textWindow);
					isStartGame = false;			
				},60500);
			}
			// В процессе игры
			else{
				let flipCards =  Array.from(document.querySelectorAll('div.non_clicked'));
				// В случае победы
				if(flipCards.length==11){
					setTimeout(() => {
						clearInterval(timerId);
						modalWindow.style.display = "flex";
						textWindow.innerHTML="Win";
						buttonWindow.innerHTML = "Play again";
						animateText(textWindow);
					}, 500);
					isWinGame = true;
					isStartGame = false;
					clearTimeout(timerGame);
				}			
			}
		});
	});
}

//Механика игры
function game({frontClass}){
	let cardsArray = Array.from(document.querySelectorAll('div.card'));
	var clickIndex = 0;
 	var elem1;
	var elem2;
	var isSimilar;
	
	cardsArray.forEach(function(cardOne) {
		cardOne.addEventListener("click", function(event) {
			if (event.target.classList.contains("card")) {				
				switch(clickIndex){
					case 0: 
						elem1 = event.target;
						elem1.classList.add("non_clicked");
						clickIndex++;
						break;
					case 1:
						elem2 = event.target;
						isSimilar = checkCards(elem1,elem2);
						clickIndex++;
						break;
					case 2:	
						if(!isSimilar){
							closeCards(elem1,elem2);							
						}
						elem1 = event.target;
						elem1.classList.add("non_clicked");
						clickIndex=1;
						break;
					default:
						alert("Error!");
				}
			}			
		});
	});
}
//закрыть карточки, которые не совпали
function closeCards(elem1,elem2) { 
		elem1.classList.remove("non_clicked");
		elem2.classList.remove("non_clicked");
		elem1.classList.remove("front-rotate");
		elem2.classList.remove("front-rotate");

		var first = elem1.querySelector('div.back');
		var second = elem2.querySelector('div.back');
		second.classList.remove("wrong_click");
		first.classList.remove("wrong_click");
	}
// проверить две карточки на совпадение эмоджи
function checkCards(elem1,elem2) {
	var first = elem1.querySelector('div.back');
	var second = elem2.querySelector('div.back');
	elem1.classList.add("non_clicked");
	elem2.classList.add("non_clicked");
	
	if(first.innerHTML==second.innerHTML){
		setTimeout(() => {  
			second.classList.add("right_click");
			first.classList.add("right_click"); }, 400);	
			return true;	
	}
	else{
		setTimeout(() => {
			second.classList.add("wrong_click");
			first.classList.add("wrong_click");}, 400);
		return false;			
	}
}
// заполнить карточки эмоджи в произвольном порядке
function fillCards() { 
	var emoji = ['🐶', '🐱', '🐞', '🐹', '🐰', '🐙','🐶', '🐱', '🐞', '🐹', '🐰', '🐙'];
	var cardFaceArray = Array.from(document.querySelectorAll('div.back'));
	cardFaceArray.forEach(function(card) {
		var index = randomInteger(0, emoji.length-1);
		card.innerHTML = emoji[index];
		emoji.splice(index, 1);
	});
}
// получить случайное число от (min-0.5) до (max+0.5)
function randomInteger(min, max) { 
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
}


